<?php
require_once 'AltoRouter.php';
$router = new AltoRouter();
$router->addRoutes(array(
    array('GET', 'story/[i:page]', 'pages/story.php', 'story'),
));

$router->setBasePath('/');

$match = $router->match(urldecode($_SERVER['REQUEST_URI']));
if ($match) {
    foreach ($match['params'] as &$param) {
        ${key($match['params'])} = $param;
    }
    require_once $match['target'];
} else {
    http_response_code(404);
    exit('Page not found');
}