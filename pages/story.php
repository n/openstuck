<?php 
require_once 'imp/db.php';

$nextpage = $page+1;
$q = "SELECT * FROM pages WHERE page = '$page'";
$q2 = "SELECT title FROM pages WHERE page = '$nextpage'";
$result = mysqli_query($db, $q);
$reesult = mysqli_query($db, $q2);
$pageinfo = mysqli_fetch_assoc($result);
$npage = mysqli_fetch_assoc($reesult);

?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <title>Homestuck - </title>
  <meta name="description" content="A tale about a boy and his friends and a game they play together. About 8,000 pages. Don't say we didn't warn you.">
  <link rel="canonical" href="https://www.homestuck.com/story/2">
  <link rel="apple-touch-icon" sizes="180x180" href="https://www.homestuck.com/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="https://www.homestuck.com/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="https://www.homestuck.com/favicon/favicon-16x16.png">
  <link rel="manifest" href="https://www.homestuck.com/favicon/manifest.json">
  <link rel="mask-icon" href="https://www.homestuck.com/favicon/safari-pinned-tab.svg" color="#6bbe4a">
  <meta name="theme-color" content="#ffffff">
  <link rel="stylesheet" href="https://use.typekit.net/kek1uwn.css">
  <link rel="stylesheet" type="text/css" href="../assets/style.css">
</head>
<body>
  <header id="site-header" role="banner">
  <div id="header-container" class="o_story-page-header row flex pad-t-0 ">
    <a href="javascript:void(0)" data-nav-open="" class="nav-btn nav-btn--center color-white" style="overflow-wrap:normal;text-decoration:none;"><i class="icon-menu"></i><span>Menu</span></a>
    <nav id="nav-container" data-nav-state="closed" role="navigation" class="o_nav-container">
      <a href="javascript:void(0)" data-nav-close="" class="nav-btn color-white pad-y-rg type-rg" style="overflow-wrap:normal;text-decoration:none;"><i class="icon-close"></i><span>Dismiss</span></a>
      <ul id="primary-nav" class="o_primary-nav style-caps weight-bold disp-ib color-white type-rg">
        <li class="o_primary-nav-item slide-candycorn div-candycorn"><a class="o_primary-nav-link" style="color:#29ff4a" href="https://www.homestuck.com/">HOME</a></li>
        <li class="o_primary-nav-item slide-candycorn pipe"><a class="o_primary-nav-link" style="color:#39d5f6" href="https://www.homestuck.com/stories">READ</a></li>
        <li class="o_primary-nav-item slide-candycorn div-candycorn"><a class="o_primary-nav-link" style="color:#39d5f6" href="https://www.homestuck.com/info-story">INFO</a></li>
        <li class="o_primary-nav-item slide-candycorn pipe"><a class="o_primary-nav-link" style="color:#f7f72a" href="https://www.homestuck.com/info-shop">SHOP</a></li>
        <li class="o_primary-nav-item slide-candycorn div-candycorn"><a class="o_primary-nav-link" style="color:#f7f72a" href="https://www.homestuck.com/info-games">GAMES</a></li>
        <li class="o_primary-nav-item slide-candycorn pipe"><a class="o_primary-nav-link" style="color:#ffb529" href="https://www.homestuck.com/news">NEWS</a></li>
        <li class="o_primary-nav-item slide-candycorn"><a class="o_primary-nav-link" style="color:#ffb529" href="https://www.homestuck.com/info-more">MORE</a></li>
      </ul>
    </nav>
    <div id="site-search" class="o_site-search disp-ib">
      <form action="/search" method="GET">
        <input name="search" type="search" class="o_search-ac o_site-search-field" placeholder="" autocomplete="off">
        <button class="o_site-search-btn"><i class="icon-search"></i></button>
      </form>
    </div>
  </div>
  </header>
  <div class="pos-r">
  <div class="row bg-hs-gray bg-light-gray--md pad-t-md--md pos-r">
    <div id="content_container" class="mar-x-auto disp-bl bg-hs-gray" style="max-width:650px;">
        <h2 class="pad-t-md pad-x-lg--md type-center type-hs-header line-tight"><?php echo $pageinfo["title"]; ?></h2>
        <div class="pad-t-md">
            <?php if($pageinfo["has-image"] = 1) {echo "<img src='".$pageinfo["image"]."' class='mar-x-auto disp-bl'>";}; ?>
        </div>
    </div>
  </div>
  <div class="row bg-hs-gray bg-light-gray--md pad-b-md pad-b-lg--md pos-r">
    <div class="mar-x-auto disp-bl bg-hs-gray pad-t-lg" style="max-width:650px;">
        <div class="o_story-nav type-hs-copy line-tight pad-x-0 pad-x-lg--md mar-b-lg">
            <div>
              <span class="">&gt;</span>
              <?php echo "<a href='".$nextpage."'>".$npage["title"]."</a>";?><br>
            </div>
        </div>   
        <footer id="story-footer" role="banner">
        <div id="story_footer_container" class="o_story-page-footer flex pad-t-0 pad-x-0 flex-justify">
        <div class="pad-l-lg--md mar-b-md type-hs-small type-hs-bottom--md type-center type-left--md">
        <ul class="o_game-nav">
          <li class="o_game-nav-item">
          <a id='o_start-over' href='1'>Start Over</a>
          </li>
            <li class="o_game-nav-item">
              <a href="https://www.homestuck.com/story/1">Go Back</a>
            </li>
        </ul>
        </div>
        </ul>
      </nav>
      <a href="javascript:void(0)" data-gamenav-open="" class="nav-btn nav-btn--center type-hs-small mar-b-md" style="text-decoration:none; text-transform: none; font-size:14px; line-height:14px;"><span>Options</span> <i class="icon-menu" style="margin:0;"></i></a>  </div>
</footer>

  
    </div>
  </div>

  
</div>

    
<div class="o_site-footer row pos-r pad-t-rg bg-dark-gray">
  <div class="float-l"><img src="Homestuck_files/footer_logo-a913b68f0efbaed8da48bc0a4f22b35d369f40d0e4db1320.gif" alt="Footer logo"><br></div>
  <div class="float-r disp-n disp-bl--sm"><img src="Homestuck_files/footer_logo-a913b68f0efbaed8da48bc0a4f22b35d369f40d0e4db1320.gif" alt="Footer logo"><br></div>
  <div class="type-center pad-t-md--md">
    <ul>
      <li><a href="https://gitdab.com/n/openstuck">Source</a></li>
      <li><a href="https://twitter.com/50dicksinmyanus">Contact</a></li>
    </ul>
    <div><span style="white-space:nowrap;">© 2018 Homestuck</span> <span style="white-space:nowrap;">and VIZ Media</span></div>
  </div>
</div>



    <div id="overlay" data-overlay-state="off" class="o_overlay"></div>
    <div id="game_overlay" data-game-overlay-state="off" class="o_game-overlay"></div>
    <div id="overlay_wait" data-overlay-state="off" class="o_overlay-wait"></div>

    </div>


  

</body></html>